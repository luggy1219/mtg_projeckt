<?php
include_once ('datenbank/db.php');
/*include_once('inc/config.inc.php');
include_once('inc/connect_mysql.inc.php');*/

if(!isset($_SESSION)) {
    session_start();
}
if(isset($_SESSION['username'])){
    $loggedin ="true";
    $loggedin_username = $_session['username'];
}else{
    $loggedin ="false";
}



if (isset($_GET['page']) && !empty($_GET['page'])) {
    $page = htmlspecialchars($_GET['page']);
} else {
    $page = 'forum/index.php';
}

require_once('inc/header.inc.php');
?>

<div class="main-container container">
<?php include ('page/' . $page . '.php');?>
</div>
<?php



require_once('inc/footer.inc.php');
//require_once('inc/app_bottom.inc.php');

?>

