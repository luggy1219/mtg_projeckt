<!doctype html>
<html lang="de">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Bootstrap CSS -->
   
    <title>Magic the Gathering</title>
    <link rel="stylsheet" href="inc/style/css/bootstrap.min.css">
    <link rel="stylsheet" href="inc/style/css/bootstrap-grid.min.css">   
    <link rel="stylsheet" href="inc/style/css/bootstrap-reboot.min.css">  
    <link rel="stylesheet" href="inc/style/style.css"> 
    <link rel="stylesheet" href="inc/style/nav.style.css"> 
    <link rel="shortcut icon" href="favicon.ico" />
    <script type="text/javascript" src="inc/style/js/bootstrap.min.js"></script>
    <?php require_once('nav.inc.php'); ?>

</head>

