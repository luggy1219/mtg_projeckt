<?php

function redirect($target)
{
    header('Location: ' . $target);
    die();
}

/**
 * @param $data
 * @return string
 */
function apiSuccessResponse($data = null)
{
    $result = array(
        'success' => true
    );
    if ($data) {
        $result['data'] = $data;
    }

    return json_encode($result);
}

/**
 * @param $message
 * @return string
 */
function apiErrorResponse($message)
{
    return json_encode(array(
        'success' => false,
        'error_message' => $message
    ));
}

/**
 * @param $message
 * @uses apiError()
 *
 * Accepts string message, which is directly printed and dies the script
 */
function abortAndPrintError($message)
{
    die(apiErrorResponse($message));
}

function mysqliResultMatrix($result)
{
    $matrix = [];
    while ($row = $result->fetch_assoc()) {
        $matrix[] = $row;
    }

    return $matrix;
}
