<?php



$username = "";
$email = "";

if (isset($_POST['username'], $_POST['email'], $_POST['password'], $_POST['password_repeat'])) {
    if ($_POST['password'] == $_POST['password_repeat']) {
        $register_statment = $con->prepare("INSERT INTO users (username, password, email, usergroup) VALUES (?, ?, ?, 'guest')");
        $username = htmlspecialchars($_POST['username']);
        $email = htmlspecialchars($_POST['email']);
        $password = password_hash(htmlspecialchars($_POST['password']), PASSWORD_DEFAULT);

        /*$login_statement = $con->prepare("SELECT * FROM user_users WHERE username LIKE :username OR email LIKE :email");
    $login_statement->bindParam("username", $username);
    $login_statement->bindParam("email", $email);
    $login_statement->execute();
    $user = $login_statement->fetch();*/

        if ($user != null) {
            $register_statment->bind_param('sss', $username, $password, $email);

            $register_statment->execute();

            echo ('<div class="alert alert-danger" role="alert"> Dein Account wurde erstellt!</div>');
        } else {
            echo ('<div class="alert alert-danger" role="alert">Benutzername oder Password wird bereits verwendet!</div>');
        }
    } else {
        $username = $_POST['username'];
        $email = $_POST['email'];
        echo ('<div class="alert alert-danger" role="alert"> Die Passwörter stimmen nicht überein!</div>');
    }
}
$_SESSION["login"] = 1;
if (isset($_SESSION["login"]) || $_SESSION["login"] != 1) {
?>

<body>
    <h1>Neues Konto eröffnen</h1>
    <hr>
    <form class="col-md-6 offset-md-3" method="POST" action="?page=registrierung">
        <div class="form-group">
            <label>Benutzername</label>
            <input type="text" class="form-control" name="username" required value="<?php echo ($username) ?>">
        </div>
        <div class="form-group">
            <label>E-Mail Adresse</label>
            <input type="email" class="form-control" name="email" required value="<?php echo ($email) ?>">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" name="password" require>
        </div>
        <div class="form-group">
            <label>Password wiederholen</label>
            <input type="password" class="form-control" name="password_repeat" require>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-outline-primary" value="Registrieren" />
        </div>
    </form>

</body>
<?php } ?>