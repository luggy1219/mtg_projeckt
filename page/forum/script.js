var defaultThreads = [
    {
        id: 1,
        title: "Commanda",
        author: "Aaron",
        date: Date.now(),
        content: "Commanda",
        comments: [
            {
                author: "Lukas",
                date: Date.now(),
                content: "Mit viele Lebenspunkte spielt man?"
            },
            {
                author: "Arthur",
                date: Date.now(),
                content: "Mit 40 Lebenspunkte"
            }
        ]
    },
    {
        id: 2,
        title: "Regelfrage",
        author: "Aaron",
        date: Date.now(),
        content: "Regelfrage",
        comments: [
            {
                author: "Jack",
                date: Date.now(),
                content: "Hey there"
            },
            {
                author: "Arthur",
                date: Date.now(),
                content: "Hey to you too"
            }
        ]
    }
]

var threads = defaultThreads
if (localStorage && localStorage.getItem('threads')) {
    threads = JSON.parse(localStorage.getItem('threads'));
} else {
    threads = defaultThreads;
    localStorage.setItem('threads', JSON.stringify(defaultThreads));
}