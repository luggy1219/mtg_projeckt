<head>
<link rel="stylesheet" href="inc/style/css/style.css">
</head>
<body>
    <div class="top-bar">
        <h1>
            Themen
        </h1>
    </div>
    <div class="main">
        <ol>
        </ol>
    </div>
    <script type="text/javascript" src='inc/style/js/script.js'></script>
    <script>
        console.log(threads);
        var container = document.querySelector('ol');
        for (let thread of threads) {
            var html = `
            <li class="row">
            <a href="?page=forum/thread?${thread.id}">
                    <h4 class="title">
                        ${thread.title}
                    </h4>
                    <div class="bottom">
                        <p class="timestamp">
                            ${new Date(thread.date).toLocaleString()}
                        </p>
                        <p class="comment-count">
                            ${thread.comments.length} comments
                        </p>
                    </div>
                </a>
            </li>
            `
            container.insertAdjacentHTML('beforeend', html);
        }
    </script>
   
</body>