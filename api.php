<?php
// Includes
require_once('inc/app_top.inc.php');

// Change Content Type
header('Content-Type: application/json');

// Action Logic
if (isset($_GET['action']) && !empty($_GET['action'])) {
    $apiAction = $_GET['action'];
} else {
    $apiAction = 'index';
}

$filename = 'api/' . $apiAction . '.php';
if (!file_exists($filename)) {
    $filename = 'api/404.php';
}

// Content
require_once($filename);

// Application Bottom
require_once('inc/app_bottom.inc.php');
