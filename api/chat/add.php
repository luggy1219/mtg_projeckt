<?php

$statment = $db->prepare("
    INSERT INTO chat
    VALUES (null, ?, ?, current_timestamp());
");
$statment->bind_param('is', $username, $message);

$username = 1;
$message = $_POST['message'];

if ($statment->execute()) {
    echo '{"success":"true"}';
} else {
    echo '{"success":"false"}';
}
