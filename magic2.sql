-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 08. Jul 2021 um 19:35
-- Server-Version: 10.4.19-MariaDB
-- PHP-Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `magic2`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `username` int(11) NOT NULL,
  `message` text NOT NULL,
  `reated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `chat`
--

INSERT INTO `chat` (`id`, `username`, `message`, `reated`) VALUES
(1, 1, 'hallo1', '2021-05-20 18:51:09');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `forum_forums`
--

CREATE TABLE `forum_forums` (
  `id` int(11) NOT NULL,
  `id_group` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `images` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `forum_forums`
--

INSERT INTO `forum_forums` (`id`, `id_group`, `name`, `description`, `images`) VALUES
(1, 1, 'test', 'test', '1');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `forum_groups`
--

CREATE TABLE `forum_groups` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `forum_groups`
--

INSERT INTO `forum_groups` (`id`, `name`) VALUES
(1, 'test');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `forum_threadposts`
--

CREATE TABLE `forum_threadposts` (
  `id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `user` text NOT NULL,
  `answer` longtext NOT NULL,
  `datetime` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `forum_threadposts`
--

INSERT INTO `forum_threadposts` (`id`, `thread_id`, `user`, `answer`, `datetime`) VALUES
(1, 1, 'lukas', 'test', '01.06.2021');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `forum_threads`
--

CREATE TABLE `forum_threads` (
  `id` int(11) NOT NULL,
  `forum_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_date` date NOT NULL,
  `last_answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `forum_threads`
--

INSERT INTO `forum_threads` (`id`, `forum_id`, `title`, `user_id`, `create_date`, `last_answer`) VALUES
(1, 1, 'hallo', 1, '2021-06-17', 'hallo');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `usergroup` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `usergroup`) VALUES
(1, 'lukas', 'lukas', '', ''),
(2, '321', '$2y$10$90pek2deuffjlXmhK/S.RuwIcu703zQgO67yvGcQP0dG32ELFWf6S', '321@312.at', 'guest');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `forum_forums`
--
ALTER TABLE `forum_forums`
  ADD PRIMARY KEY (`id_group`);

--
-- Indizes für die Tabelle `forum_groups`
--
ALTER TABLE `forum_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `forum_threadposts`
--
ALTER TABLE `forum_threadposts`
  ADD PRIMARY KEY (`thread_id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `forum_forums`
--
ALTER TABLE `forum_forums`
  ADD CONSTRAINT `forum_forums_ibfk_1` FOREIGN KEY (`id`) REFERENCES `forum_groups` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
